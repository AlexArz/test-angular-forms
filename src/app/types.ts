export type State = {
  id: number;
  name: string;
  goods: Array<Good>;
};

export type Good = {
  id: number;
  name: string;
  date: string;
  price: number;
  connected: boolean;
};

export type FormData = {
  name: string;
  username: string;
  notif: boolean;
  email: string;
  tel: string;
  radio: string;
};

export type User = {
  id: number;
  name: string;
  goods: Array<Good>;
  price: number;
};

export type IdChanged = {
  userId: number;
  goodId?: number;
  goodArr?: Array<number>;
};
