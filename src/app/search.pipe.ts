import { Good } from './types';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search',
})
export class SearchPipe implements PipeTransform {
  transform(goods: Array<Good>, search: string): Array<Good> {
    if (!search.trim()) {
      return goods;
    }
    return goods.filter((el) => el.name.toLowerCase().includes(search));
  }
}
