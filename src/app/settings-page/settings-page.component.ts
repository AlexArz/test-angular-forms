import { FormData } from './../types';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';
import { MyValidators } from '../my.validators';

@Component({
  selector: 'app-settings-page',
  templateUrl: './settings-page.component.html',
  styleUrls: ['./settings-page.component.scss'],
})
export class SettingsPageComponent implements OnInit {
  form: FormGroup;
  data: FormData;
  constructor(public mainService: MainService) {}

  ngOnInit(): void {
    this.init();
  }

  init(): void {
    this.data =
      this.mainService.getDataSettingsPage() || this.mainService.initialForm;
    this.form = new FormGroup({
      name: new FormControl(this.data.name, Validators.maxLength(200)),
      username: new FormControl(this.data.username, Validators.maxLength(200)),
      notif: new FormControl(this.data.notif),
      email: new FormControl(this.data.email, [
        Validators.email,
        Validators.maxLength(200),
      ]),
      tel: new FormControl(this.data.tel, [
        Validators.maxLength(11),
        MyValidators.phoneCheck,
        Validators.required,
      ]),
      radio: new FormControl(this.data.radio),
    });
  }

  checkForm(form: FormData): boolean {
    return JSON.stringify(form) === JSON.stringify(this.data);
  }

  submit(): void {
    const data = { ...this.form.value };
    this.data = data;
    this.mainService.postDataSettingsPage(data);
  }
}
