import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { SettingsPageComponent } from './settings-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [SettingsPageComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: SettingsPageComponent }]),
  ],
})
export class SettingsPageModule {}
