import { FormControl } from '@angular/forms';

export class MyValidators {
  static phoneCheck(control: FormControl): { [key: string]: boolean } {
    if (control.value[0] !== '8' || control.value[1] !== '9') {
      return { phoneMistake: true };
    }
    return null;
  }
}
