import { RouterModule } from '@angular/router';
import { SearchPipe } from './../search.pipe';
import { NgModule } from '@angular/core';
import { HomePageComponent } from './home-page.component';
import { LeftColumnComponent } from './left-column/left-column.component';
import { RightColumnComponent } from './right-column/right-column.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    HomePageComponent,
    LeftColumnComponent,
    RightColumnComponent,
    SearchPipe,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: HomePageComponent }]),
  ],
})
export class HomePageModule {}
