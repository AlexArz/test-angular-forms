import { User, State, IdChanged } from './../../types';
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnChanges,
} from '@angular/core';
import { MainService } from 'src/app/main.service';

@Component({
  selector: 'app-left-column',
  templateUrl: './left-column.component.html',
  styleUrls: ['./left-column.component.scss'],
})
export class LeftColumnComponent implements OnInit, OnChanges {
  users: Array<User> = [];
  data: Array<State> = [];
  selectedId: number;

  @Output() onSelected = new EventEmitter();
  @Input() idChanged: IdChanged;

  constructor(public mainService: MainService) {}

  ngOnInit(): void {
    this.data =
      this.mainService.getDataHomePage() || this.mainService.initialState;
    this.changeData();
    this.selectedId = this.users[0].id;
  }

  ngOnChanges(): void {
    if (this.idChanged) {
      if (this.idChanged.goodId) {
        this.data
          .find((el) => el.id === this.idChanged.userId)
          .goods.find((g) => g.id === this.idChanged.goodId).connected = false;
      }

      if (this.idChanged.goodArr) {
        this.idChanged.goodArr.forEach((val) => {
          if (this.data.find((el) => el.id === this.idChanged.userId)) {
            this.data
              .find((el) => el.id === this.idChanged.userId)
              .goods.find((g) => g.id === val).connected = true;
          }
        });
      }
    }
    this.changeData();
  }

  selectedUser(id: number): void {
    this.selectedId = id;
    this.onSelected.emit(id);
  }

  changeData(): void {
    this.users = this.data.map((el) => {
      return {
        id: el.id,
        name: el.name,
        goods: el.goods.filter((el) => el.connected),
        price: el.goods
          .filter((el) => el.connected)
          .reduce((acc, val) => acc + val.price, 0),
      };
    });
  }
}
