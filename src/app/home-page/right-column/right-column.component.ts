import { State, Good } from './../../types';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
} from '@angular/core';
import { MainService } from 'src/app/main.service';

@Component({
  selector: 'app-right-column',
  templateUrl: './right-column.component.html',
  styleUrls: ['./right-column.component.scss'],
})
export class RightColumnComponent implements OnInit, OnChanges {
  data: Array<State>;
  user: State;
  id: number;
  disconnected: Array<Good> = [];
  connected: Array<Good> = [];
  checkedGoods: Array<number> = [];
  filter: string = '';

  @Input() changedId: number;
  @Output() changedGoods = new EventEmitter();
  constructor(public mainService: MainService) {}
  ngOnChanges(): void {
    if (this.changedId) {
      this.id = this.changedId;
      this.user = this.data.find((el) => el.id === this.changedId);
    }
  }

  ngOnInit(): void {
    this.data =
      this.mainService.getDataHomePage() || this.mainService.initialState;
    this.user = this.data[0];
    this.id = this.user.id;
    this.checkGoods();
  }

  remove(id: number): void {
    this.user.goods.find((el) => el.id === id).connected = false;
    this.changedGoods.emit({ userId: this.id, goodId: id });
    this.data.find((el) => el.id === this.id).goods = this.user.goods;
    this.checkGoods();
    this.mainService.postDataHomePage(this.data);
  }

  onChangeCheckbox(evt, id: number): void {
    if (evt.target.checked) {
      this.checkedGoods.push(id);
    } else {
      const idx = this.checkedGoods.findIndex((el) => el === id);
      this.checkedGoods.splice(idx, 1);
    }
  }

  addGoods(): void {
    this.checkedGoods.forEach((id) => {
      if (this.user.goods.find((el) => el.id === id)) {
        this.user.goods.find((el) => el.id === id).connected = true;
        this.user.goods.find(
          (el) => el.id === id
        ).date = new Date().toLocaleDateString();
      }
    });
    this.changedGoods.emit({ userId: this.id, goodArr: this.checkedGoods });
    this.data.find((el) => el.id === this.id).goods = this.user.goods;
    this.mainService.postDataHomePage(this.data);
    this.checkGoods();
    this.checkedGoods = [];
  }

  checkGoods(): void {
    this.disconnected = this.user.goods.filter((e) => !e.connected);
    this.connected = this.user.goods.filter((e) => e.connected);
  }
}
