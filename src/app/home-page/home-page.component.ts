import { Good } from './../types';
import { Component } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent {
  id: number;
  good: Good;
  number: number;

  changeId(evt: number): void {
    this.id = evt;
  }

  changeGoods(evt: Good): void {
    this.good = evt;
  }
}
