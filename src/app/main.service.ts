import { Injectable } from '@angular/core';
import { FormData, State } from './types';

@Injectable({
  providedIn: 'root',
})
export class MainService {
  initialState: Array<State> = [
    {
      id: 1,
      name: 'Элвисова Женя Сергеевна',
      goods: [
        {
          id: 1,
          name: 'Мобильный помощник',
          date: '20.04.2020',
          price: 0,
          connected: true,
        },
        {
          id: 2,
          name: 'Определитель номера',
          date: '20.04.2020',
          price: 1000,
          connected: true,
        },
        {
          id: 3,
          name: 'Служба коротких сообщений',
          date: '20.04.2020',
          price: 0,
          connected: true,
        },
        {
          id: 4,
          name: 'Мобильный интернет',
          date: '',
          price: 200,
          connected: false,
        },
        {
          id: 5,
          name: 'Конференц-связь',
          date: '',
          price: 0,
          connected: false,
        },
      ],
    },
    {
      id: 2,
      name: 'Александрова Рэй Алексеевна',
      goods: [
        {
          id: 1,
          name: 'Мобильный помощник',
          date: '20.04.2020',
          price: 0,
          connected: true,
        },
        {
          id: 2,
          name: 'Определитель номера',
          date: '',
          price: 1000,
          connected: false,
        },
        {
          id: 3,
          name: 'Мобильный интернет',
          date: '',
          price: 200,
          connected: false,
        },
        {
          id: 4,
          name: 'Конференц-связь',
          date: '',
          price: 0,
          connected: false,
        },
      ],
    },
  ];

  initialForm: FormData = {
    name: 'Доронин Сергей Леонидыч',
    username: 'Псевдоним',
    notif: true,
    email: 'a@a.ru',
    tel: '89005555555',
    radio: 'email',
  };
  constructor() {}

  postDataHomePage(data: Array<State>): void {
    localStorage.setItem('home-page', JSON.stringify(data));
  }

  getDataHomePage(): Array<State> {
    return JSON.parse(localStorage.getItem('home-page'));
  }

  postDataSettingsPage(data: FormData): void {
    localStorage.setItem('settings-page', JSON.stringify(data));
  }

  getDataSettingsPage(): FormData {
    return JSON.parse(localStorage.getItem('settings-page'));
  }
}
